package com.delicloud.app.uidemo.fragment

import com.delicloud.app.uidemo.R
import com.delicloud.app.uidemo.base.BaseFragment

/**
 * 布局页面
 * @author wangchangwei
 * @create 2019/7/15 15:15
 * @Describe
 */
class EntryFragment : BaseFragment() {
    override fun layoutId(): Int = R.layout.fragment_entry

    override fun initView() {

    }

    override fun initData() {
    }

}