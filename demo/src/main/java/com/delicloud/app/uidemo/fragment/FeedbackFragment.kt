package com.delicloud.app.uidemo.fragment

import android.util.Log
import androidx.core.content.ContextCompat
import com.delicloud.app.uidemo.R
import com.delicloud.app.uidemo.base.BaseFragment
import com.delicloud.app.uikit.feedback.dialog.DeiUiDialogFragmentHelper
import com.delicloud.app.uikit.feedback.dialog.DeiUiListItemDialogFragment
import kotlinx.android.synthetic.main.fragment_feedback.*

/**
 * 布局页面
 * @author wangchangwei
 * @create 2019/7/15 15:15
 * @Describe
 */
class FeedbackFragment : BaseFragment() {
    override fun layoutId(): Int = R.layout.fragment_feedback

    override fun initView() {
        single_btn_dialog_btn.setOnClickListener { showOneButton() }
        double_btn_dialog_btn.setOnClickListener { showDoubleButton() }
        multi_btn_dialog_btn.setOnClickListener { showMultiBtnDialog(true) }
        multi_btn_list_dialog_btn.setOnClickListener { showMultiBtnDialog(false) }
        without_title_dialog_btn.setOnClickListener { showDoubleButtonWithoutTitle() }
        with_img_dialog_btn.setOnClickListener {showDialogWithImg() }
    }

    fun showOneButton() {
        DeiUiDialogFragmentHelper.createOneButtonDialog(context, "单行标题", "描述文字的字数最好控制在三行", "确定", true) {
            Log.d("TTTT", "点击了aaaaa")

        }
            .show(fragmentManager,"单按钮")
    }

    fun showDoubleButton() {
        val dialog = DeiUiDialogFragmentHelper.createDoubleButtonWithTitleDialog(
            context,
            getString(R.string.DialogTitle),
            getString(R.string.DialogMessage),
            "主操作",
            "辅助操作",
            true,
            null
        )
        dialog.show(fragmentManager,"两个按钮")
    }

    /*
    * withTitleMessage 是否带标题和描述信息
    * */
    fun showMultiBtnDialog(withTitleMessage: Boolean) {
        val dialog = DeiUiListItemDialogFragment(activity)
            .apply {
                if (withTitleMessage) {
                    setTitle(this@FeedbackFragment.getString(R.string.DialogTitle))
                    setMessage(this@FeedbackFragment.getString(R.string.DialogMessage))
                }
                addItem("操作1", {})
                addItem("操作2", {})
                addItem("操作3", {})
            }
        dialog.show(fragmentManager,"列表")
    }

    fun showDialogWithImg(){
        val dialog = DeiUiDialogFragmentHelper.createOneButtonWithImgDialog(
            context,
            getString(R.string.DialogTitle),
            getString(R.string.DialogMessage),
            getString(R.string.DialogPositiveText),
            true,
            null
        )
        dialog.setDrawable(ContextCompat.getDrawable(context!!,R.drawable.ic_tab_bar_navigation))
        dialog.show(fragmentManager,"")
    }

    fun showDoubleButtonWithoutTitle() {
        val dialog = DeiUiDialogFragmentHelper.createDoubleButtonWithoutTittleDialog(
            context,
            getString(R.string.DialogMessage),
            getString(R.string.DialogPositiveText),
            getString(R.string.DialogNegativeText),
            true,
            null
        )
        dialog.show(fragmentManager,"双按钮不带标题")

    }

    override
    fun initData() {
    }

}