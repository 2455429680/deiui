package com.delicloud.app.uidemo

import android.graphics.Color
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.delicloud.app.uidemo.extentions.contentView
import com.delicloud.app.uidemo.extentions.setupWithViewPager
import com.delicloud.app.uidemo.fragment.*
import com.delicloud.app.uidemo.utils.getStatusBarHeight
import com.delicloud.app.uidemo.utils.setStatusTransAndDarkIcon
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.topPadding

/**
 * 主页
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    fun initView() {
        setStatusTransAndDarkIcon(Color.WHITE)
        contentView?.topPadding = getStatusBarHeight()
        bottom_nav.setItemIconTintList(null);
        view_pager.adapter = MainPagerAdapter(supportFragmentManager)
        view_pager.offscreenPageLimit = 3
        bottom_nav.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED
        bottom_nav.setupWithViewPager(view_pager)
    }


    inner class MainPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> LayoutFragment()
                1 -> NavigationFragment()
                2 -> EntryFragment()
                3 -> DisplayFragment()
                4 -> FeedbackFragment()
                else -> LayoutFragment()
            }
        }

        override fun getCount() = 5
    }

}
