package com.delicloud.app.uidemo

import android.app.Application
import com.delicloud.app.uikit.utils.ScreenUtil

/**
 *Created By Mr.m
 *2019/7/17
 **/
class DemoApplication:Application() {
    override fun onCreate() {
        super.onCreate()
        ScreenUtil.init(this)
        instance=this
    }
    companion object{
        lateinit var instance:Application
    }
}