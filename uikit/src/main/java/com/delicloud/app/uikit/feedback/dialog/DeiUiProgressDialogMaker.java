package com.delicloud.app.uikit.feedback.dialog;

import android.content.Context;
import android.content.DialogInterface.OnCancelListener;
import android.text.TextUtils;
import android.util.Log;

public class DeiUiProgressDialogMaker {
    private static DeiUiProgressDialog progressDialog;

    public static DeiUiProgressDialog showProgressDialog(Context context, String message) {
        return showProgressDialog(context, null, message, true, null);
    }

    public static DeiUiProgressDialog showProgressDialog(Context context, String message, boolean cancelable) {
        return showProgressDialog(context, null, message, cancelable, null);
    }

    @Deprecated
    public static DeiUiProgressDialog showProgressDialog(Context context,
                                                         String title, String message, boolean canCancelable, OnCancelListener listener) {

        if (progressDialog == null) {
            progressDialog = new DeiUiProgressDialog(context, message);
        } else if (progressDialog.getContext() != context) {
            // maybe existing dialog is running in a destroyed activity cotext
            // we should recreate one
            Log.e("dialog", "there is a leaked window here,orign context: "
                    + progressDialog.getContext() + " now: " + context);
            dismissProgressDialog();
            progressDialog = new DeiUiProgressDialog(context, message);
        }

        progressDialog.setCancelable(canCancelable);
        progressDialog.setOnCancelListener(listener);

        progressDialog.show();

        return progressDialog;
    }

    public static void dismissProgressDialog() {
        if (null == progressDialog) {
            return;
        }
        if (progressDialog.isShowing()) {
            try {
                progressDialog.dismiss();
                progressDialog = null;
            } catch (Exception e) {
                // maybe we catch IllegalArgumentException here.
            }

        }

    }

    public static void setMessage(String message) {
        if (null != progressDialog && progressDialog.isShowing()
                && !TextUtils.isEmpty(message)) {
            progressDialog.setMessage(message);
        }
    }

    public static void updateLoadingMessage(String message) {
        if (null != progressDialog && progressDialog.isShowing()
                && !TextUtils.isEmpty(message)) {
            progressDialog.updateLoadingMessage(message);
        }
    }

    public static boolean isShowing() {
        return (progressDialog != null && progressDialog.isShowing());
    }
}
