package com.delicloud.app.uikit.navigation.model;

import android.graphics.drawable.Drawable;

/**
 * This entity used to receive data to add into the popup window
 * Created by Irvin on 2017/8/5.
 */

public class DeiUiMenuPopupWindowModel {

    private int imageResource;
    private Drawable imageDrawable;
    private String textString;
    private int textInt;
    private int textColor;

    public DeiUiMenuPopupWindowModel() {
    }

    public int getImageResource() {
        return imageResource;
    }

    public void setImageResource(int imageResource) {
        this.imageResource = imageResource;
    }

    public Drawable getImageDrawable() {
        return imageDrawable;
    }

    public void setImageDrawable(Drawable imageDrawable) {
        this.imageDrawable = imageDrawable;
    }

    public String getTextString() {
        return textString;
    }

    public void setTextString(String textString) {
        this.textString = textString;
    }

    public int getTextInt() {
        return textInt;
    }

    public void setTextInt(int textInt) {
        this.textInt = textInt;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }
}
