package com.delicloud.app.uikit.feedback.dialog;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.*;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import com.delicloud.app.uikit.R;
import com.delicloud.app.uikit.utils.ScreenUtil;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * 普通提示包含两个按钮以及Title和Message(居中显示).
 * <p/>
 * 警告提示使用包含一个按钮以及Title和Message(居中显示).
 * <p/>
 * 错误提示使用包含一个按钮(红色背景)以及Title和Message(居中显示).
 * <p/>
 * 特殊布局需求可以自定义布局.
 */
public class DeiUiDialogFragment extends DialogFragment {
    private Context context;
    private int style;

    public static final int NO_TEXT_COLOR = -99999999;

    public static final int NO_TEXT_SIZE = -99999999;

    private TextView titleTV;

    private TextView messageTV;
    private ImageView imageView;
    private View rootView;
    private Drawable drawable;
    private Bitmap bitmap;


    private TextView positiveTv, negativeTv;

    private CharSequence title = "", message = "", message2 = "", positiveBtnTitle = "", negativeBtnTitle = "";

    private int titleTextColor = NO_TEXT_COLOR, msgTextColor = NO_TEXT_COLOR,
            positiveBtnTitleTextColor = NO_TEXT_COLOR, negativeBtnTitleTextColor = NO_TEXT_COLOR;

    private float titleTextSize = NO_TEXT_SIZE, msgTextSize = NO_TEXT_SIZE, positiveBtnTitleTextSize = NO_TEXT_SIZE,
            negativeBtnTitleTextSize = NO_TEXT_SIZE;

    private int resourceId;

    private boolean isPositiveBtnVisible = true, isNegativeBtnVisible = false;

    private boolean isTitleVisible = false, isMessageVisble = true, isTitleBtnVisible = false;

    private View.OnClickListener positiveBtnListener, negativeBtnListener;

    private HashMap<Integer, View.OnClickListener> mViewListener = new HashMap<Integer, View.OnClickListener>();

    public DeiUiDialogFragment(Context context, int resourceId, int style) {
        super();
        this.context = context;
        if (-1 != resourceId) {
            this.resourceId = resourceId;
        }
        this.style = style;
    }

    public DeiUiDialogFragment(Context context) {
        this(context, R.layout.deiui_alert_dialog_default);
    }

    public DeiUiDialogFragment(Context context, int resourceId) {
        this(context, resourceId, R.style.dialog_default_style);
    }


    public void setTitle(CharSequence title) {
        isTitleVisible = !TextUtils.isEmpty(title);
        setTitleVisible(isTitleVisible);
        if (null != title) {
            this.title = title;
            if (null != titleTV) {
                titleTV.setText(title);
            }
        }
    }

    public void setDrawable(Drawable drawable) {
        this.drawable = drawable;
        if (imageView != null) {
            imageView.setImageDrawable(drawable);
        }
    }

    public void setDrawableRes(int res) {
        setDrawable(ContextCompat.getDrawable(getContext(), res));
    }

    public void setImageBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
        if (imageView != null) {
            imageView.setImageBitmap(bitmap);
        }
    }

    public void setTitleVisible(boolean visible) {
        isTitleVisible = visible;
        if (titleTV != null) {
            titleTV.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setTitleTextColor(int color) {
        titleTextColor = color;
        if (null != titleTV && NO_TEXT_COLOR != color) {
            titleTV.setTextColor(color);
        }
    }

    public void setMessageTextColor(int color) {
        msgTextColor = color;
        if (null != messageTV && NO_TEXT_COLOR != color) {
            messageTV.setTextColor(color);
        }

    }

    public void setMessageTextSize(float size) {
        msgTextSize = size;
        if (null != messageTV && NO_TEXT_SIZE != size) {
            messageTV.setTextSize(size);
        }
    }

    public void setTitleTextSize(float size) {
        titleTextSize = size;
        if (null != titleTV && NO_TEXT_SIZE != size) {
            titleTV.setTextSize(size);
        }
    }

    public void setMessageVisible(boolean visible) {
        isMessageVisble = visible;
        if (messageTV != null) {
            messageTV.setVisibility(visible ? View.VISIBLE : View.GONE);
        }
    }

    public void setMessage(CharSequence message) {
        if (null != message) {
            this.message = message;
            if (null != messageTV) {
                messageTV.setText(message);
            }
        }
    }

    public void addPositiveButton(CharSequence title, int color, float size,
                                  View.OnClickListener positiveBtnListener) {
        isPositiveBtnVisible = true;
        positiveBtnTitle = TextUtils.isEmpty(title) ? context
                .getString(R.string.confirm) : title;
        positiveBtnTitleTextColor = color;
        positiveBtnTitleTextSize = size;
        this.positiveBtnListener = positiveBtnListener;
        if (positiveTv != null) {
            positiveTv.setText(positiveBtnTitle);
            positiveTv.setTextColor(positiveBtnTitleTextColor);
            positiveTv.setTextSize(positiveBtnTitleTextSize);
            positiveTv.setOnClickListener(positiveBtnListener);
        }
    }

    public void addNegativeButton(CharSequence title, int color, float size,
                                  View.OnClickListener negativeBtnListener) {
        isNegativeBtnVisible = true;
        negativeBtnTitle = TextUtils.isEmpty(title) ? context
                .getString(R.string.cancel) : title;
        negativeBtnTitleTextColor = color;
        negativeBtnTitleTextSize = size;
        this.negativeBtnListener = negativeBtnListener;

        if (negativeTv != null) {
            negativeTv.setText(negativeBtnTitle);
            negativeTv.setTextColor(negativeBtnTitleTextColor);
            negativeTv.setTextSize(negativeBtnTitleTextSize);
            negativeTv.setOnClickListener(negativeBtnListener);
        }
    }

    public void addPositiveButton(CharSequence title,
                                  View.OnClickListener positiveBtnListener) {
        addPositiveButton(title, NO_TEXT_COLOR, NO_TEXT_SIZE,
                positiveBtnListener);
    }

    public void addNegativeButton(CharSequence title,
                                  View.OnClickListener negativeBtnListener) {
        addNegativeButton(title, NO_TEXT_COLOR, NO_TEXT_SIZE,
                negativeBtnListener);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = this.getDialog().getWindow();
        //去掉dialog默认的padding
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = ScreenUtil.getDialogWidth();
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        window.setBackgroundDrawable(new ColorDrawable());
        rootView = inflater.inflate(resourceId, null);
        return rootView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, style);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
    }

    private void initView() {
        try {
            titleTV = (TextView) rootView.findViewById(R.id.title_tv);
            if (titleTV != null) {
                titleTV.setText(title);
                setTitleVisible(isTitleVisible);
                if (NO_TEXT_COLOR != titleTextColor) {
                    titleTV.setTextColor(titleTextColor);
                }
                if (NO_TEXT_SIZE != titleTextSize) {
                    titleTV.setTextSize(titleTextSize);
                }
            }
            messageTV = (TextView) rootView.findViewById(R.id.desc_tv);
            if (messageTV != null) {
                messageTV.setText(message);
                setMessageVisible(isMessageVisble);
                if (NO_TEXT_COLOR != msgTextColor) {
                    messageTV.setTextColor(msgTextColor);
                }
                if (NO_TEXT_SIZE != msgTextSize) {
                    messageTV.setTextSize(msgTextSize);
                }
            }
            positiveTv = rootView.findViewById(R.id.positive_tv);
            if (isPositiveBtnVisible && positiveTv != null) {
                positiveTv.setVisibility(View.VISIBLE);
                if (NO_TEXT_COLOR != positiveBtnTitleTextColor) {
                    positiveTv.setTextColor(positiveBtnTitleTextColor);
                }
                if (NO_TEXT_SIZE != positiveBtnTitleTextSize) {
                    positiveTv.setTextSize(positiveBtnTitleTextSize);
                }
                positiveTv.setText(positiveBtnTitle);
                positiveTv.setOnClickListener(positiveBtnListener);
            }

            negativeTv = rootView.findViewById(R.id.negative_tv);
            if (isNegativeBtnVisible) {
                negativeTv.setVisibility(View.VISIBLE);
                if (NO_TEXT_COLOR != this.negativeBtnTitleTextColor) {
                    negativeTv.setTextColor(negativeBtnTitleTextColor);
                }
                if (NO_TEXT_SIZE != this.negativeBtnTitleTextSize) {
                    negativeTv.setTextSize(negativeBtnTitleTextSize);
                }
                negativeTv.setText(negativeBtnTitle);
                negativeTv.setOnClickListener(negativeBtnListener);
            }
            imageView = rootView.findViewById(R.id.dialog_iv);
            if (imageView != null) {
                if (bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                }
                if (drawable != null) {
                    imageView.setImageDrawable(drawable);
                }
            }
            if (mViewListener != null && mViewListener.size() != 0) {
                Iterator iter = mViewListener.entrySet().iterator();
                View view = null;
                while (iter.hasNext()) {
                    Map.Entry<Integer, View.OnClickListener> entry = (Map.Entry) iter.next();
                    view = rootView.findViewById(entry.getKey());
                    if (view != null && entry.getValue() != null) {
                        view.setOnClickListener(entry.getValue());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public TextView getPositiveTv() {
        return positiveTv;
    }

    public TextView getNegativeTv() {
        return negativeTv;
    }

    public void setViewListener(int viewId, View.OnClickListener listener) {
        mViewListener.put(viewId, listener);
    }
}
