package com.delicloud.app.uikit.feedback.dialog;

import android.util.Pair;
import android.view.Gravity;
import android.widget.TextView;
import androidx.core.content.ContextCompat;
import com.delicloud.app.uikit.R;
import com.delicloud.app.uikit.feedback.adapter.TViewHolder;
import com.delicloud.app.uikit.utils.ScreenUtil;

public class DeiUiListDialogViewHolder extends TViewHolder {

    public static int TEXT_APPEARANCE_RES_ID = R.style.list_with_title_dialog_message_text_style;
    private TextView itemView;


    @Override
    protected int getResId() {
        return R.layout.deiui_list_item_dialog_item;
    }

    @Override
    protected void inflate() {
        itemView = (TextView) view.findViewById(R.id.custom_dialog_text_view);
        if (itemView != null) {
            setTextViewAttr();
        }

    }


    @Override
    protected void refresh(Object item) {
        if (item instanceof Pair<?, ?>) {
            Pair<String, Integer> pair = (Pair<String, Integer>) item;
            itemView.setText(pair.first);
            itemView.setTextColor(pair.second);
        }
      setTextViewAttr();
    }

    private void setTextViewAttr() {
        itemView.setTextAppearance(itemView.getContext(), TEXT_APPEARANCE_RES_ID);
        if(TEXT_APPEARANCE_RES_ID==R.style.list_with_title_dialog_message_text_style) {
            itemView.setGravity(Gravity.RIGHT|Gravity.CENTER_VERTICAL);
            itemView.setPadding(0,0, ScreenUtil.dip2px(23),0);
            //取消点击效果
            itemView.setBackground(null);
        }else{
            itemView.setGravity(Gravity.LEFT|Gravity.CENTER_VERTICAL);
            itemView.setPadding(ScreenUtil.dip2px(23),0,0,0);
            //设置点击效果
            itemView.setBackground(ContextCompat.getDrawable(itemView.getContext(),R.drawable.dialog_list_item_selector));
        }
    }

}
