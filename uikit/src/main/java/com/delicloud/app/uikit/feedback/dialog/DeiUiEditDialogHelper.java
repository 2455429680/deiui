package com.delicloud.app.uikit.feedback.dialog;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import com.delicloud.app.uikit.R;

/**
 * Created by Irvin
 * Date: on 2018/7/6 0006.
 */

public class DeiUiEditDialogHelper {

    public static DeiUiEditDialog createEditDialog(Context context, CharSequence titleString, CharSequence hintString, boolean cancelable, final OnDialogActionListener listener) {
        final DeiUiEditDialog dialog = new DeiUiEditDialog(context);
        dialog.setTitle((String) titleString);
        dialog.setEditHint((String) hintString);
        dialog.addNegativeButtonListener(R.string.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                listener.doCancelAction();
            }
        });
        dialog.addPositiveButtonListener(R.string.confirm, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                listener.doOkAction(dialog.getEditMessage());

            }
        });
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public static DeiUiEditDialog createEditDialog(Context context, CharSequence titleString, CharSequence hintString, CharSequence confirmString, CharSequence cancelString, boolean cancelable, final OnDialogActionListener listener) {
        final DeiUiEditDialog dialog = new DeiUiEditDialog(context);
        dialog.setTitle((String) titleString);
        dialog.setEditHint((String) hintString);
        dialog.setNegativeBtnText(cancelString);
        dialog.setPositiveBtnText(confirmString);
        dialog.addNegativeButtonListener(R.string.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                listener.doCancelAction();
            }
        });
        dialog.addPositiveButtonListener(R.string.confirm, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                listener.doOkAction(dialog.getEditMessage());

            }
        });
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public static DeiUiEditDialog createEditDialog(Context context, CharSequence titleString, CharSequence hintString, CharSequence editString, boolean cancelable, final OnDialogActionListener listener) {
        final DeiUiEditDialog dialog = new DeiUiEditDialog(context);
        dialog.setTitle((String) titleString);
        if (!TextUtils.isEmpty(editString)) {
            dialog.setEditHint((String) hintString);
        }
        dialog.setEditText((String) editString);
        dialog.addNegativeButtonListener(R.string.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                listener.doCancelAction();
            }
        });
        dialog.addPositiveButtonListener(R.string.confirm, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                listener.doOkAction(dialog.getEditMessage());

            }
        });
        dialog.setCancelable(cancelable);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    public interface OnDialogActionListener {
        void doCancelAction();

        void doOkAction(String msg);
    }
}
