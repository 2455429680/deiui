package com.delicloud.app.uikit.feedback.dialog;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Pair;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import com.delicloud.app.uikit.R;
import com.delicloud.app.uikit.feedback.adapter.TAdapter;
import com.delicloud.app.uikit.feedback.adapter.TAdapterDelegate;
import com.delicloud.app.uikit.feedback.adapter.TViewHolder;
import com.delicloud.app.uikit.utils.ScreenUtil;

import java.util.LinkedList;
import java.util.List;

public class DeiUiListItemDialogFragment extends DialogFragment {

    private Context context;

    private int itemSize = 0;

    private View titleView;
    
    private View rootView;

    private TextView titleTextView,messageTextView;

    private ImageButton titleBtn;

    private ListView listView;

    private boolean isTitleVisible = false;

    private boolean isTitleBtnVisible = false;
    private boolean isMessageVisible=false;

    private String title,message;

    private View.OnClickListener titleListener = null;

    private List<Pair<String, Integer>> itemTextList = new LinkedList<Pair<String, Integer>>();

    private List<onSeparateItemClickListener> itemListenerList = new LinkedList<onSeparateItemClickListener>();

    private View.OnClickListener listListener;

    private BaseAdapter listAdapter;

    private OnItemClickListener itemListener;
    private int defaultColor = R.color.deiui_high_level_text_color;

    public DeiUiListItemDialogFragment(Context context) {
        this.context = context;
        initAdapter();
    }

    public DeiUiListItemDialogFragment(Context context, int itemSize) {
        this.context = context;
        this.itemSize = itemSize;
    }

    private void initAdapter() {
        listAdapter = new TAdapter<>(context, itemTextList, new TAdapterDelegate() {

            @Override
            public int getViewTypeCount() {
                return itemTextList.size();
            }

            @Override
            public Class<? extends TViewHolder> viewHolderAtPosition(int position) {
                return DeiUiListDialogViewHolder.class;
            }

            @Override
            public boolean enabled(int position) {
                return true;
            }
        });
        itemListener = new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                itemListenerList.get(position).onClick();
                dismiss();
            }
        };
    }

    public void setAdapter(final BaseAdapter adapter, final View.OnClickListener listener) {
        listAdapter = adapter;
        listListener = listener;
        itemListener = new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                dismiss();
                listListener.onClick(view);
            }
        };
    }

    public void setAdapter(final BaseAdapter adapter, final OnItemClickListener listener) {
        listAdapter = adapter;
        itemListener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        Window window = this.getDialog().getWindow();
        //去掉dialog默认的padding
        window.getDecorView().setPadding(0, 0, 0, 0);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = ScreenUtil.getDialogWidth();
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        window.setBackgroundDrawable(new ColorDrawable());
        rootView = inflater.inflate(R.layout.deiui_alert_dialog_with_listview,null);
        initView();
        return rootView;
    }
    private void initView() {
        LinearLayout root = (LinearLayout) rootView.findViewById(R.id.easy_alert_dialog_layout);
       /* ViewGroup.LayoutParams params = root.getLayoutParams();
        params.width = (int) ScreenUtil.getDialogWidth();
        root.setLayoutParams(params);
        addFootView(root);*/
        titleView = rootView.findViewById(R.id.easy_dialog_title_view);
        if (titleView != null) {
            setTitleVisible(isTitleVisible);
        }
        titleTextView = (TextView) rootView.findViewById(R.id.title_tv);
        if (titleTextView != null) {
            setTitle(title);
        }
        messageTextView=(TextView) rootView.findViewById(R.id.desc_tv);
        if(messageTextView!=null){
            setMessage(message);
        }
        titleBtn = (ImageButton) rootView.findViewById(R.id.easy_dialog_title_button);
        if (titleBtn != null) {
            setTitleBtnVisible(isTitleBtnVisible);
            setTitleBtnListener(titleListener);
        }
        listView = (ListView) rootView.findViewById(R.id.easy_dialog_list_view);

        if(!isMessageVisible&&!isTitleVisible){
            //标题和描述信息都不可见时，去掉标题描述信息，更改文字样式
            titleView.setVisibility(View.GONE);
            DeiUiListDialogViewHolder.TEXT_APPEARANCE_RES_ID =R.style.list_without_title_dialog_message_text_style;
        }else{
            DeiUiListDialogViewHolder.TEXT_APPEARANCE_RES_ID=R.style.list_with_title_dialog_message_text_style;
        }
        if (itemSize > 0) {
            updateListView();
        }
    }

    protected void addFootView(LinearLayout parent) {

    }

    public void setTitle(String title) {
        this.title = title;
        isTitleVisible = TextUtils.isEmpty(title) ? false : true;
        setTitleVisible(isTitleVisible);
        if (isTitleVisible && titleTextView != null) {
            titleTextView.setText(title);
        }
    }

    public void setTitle(int resId) {
        this.title = context.getString(resId);
        isTitleVisible = TextUtils.isEmpty(title) ? false : true;
        setTitleVisible(isTitleVisible);
        if (isTitleVisible && titleTextView != null) {
            titleTextView.setText(title);
        }
    }

    public void setTitleVisible(boolean visible) {
        isTitleVisible = visible;
        if (titleView != null) {
            titleView.setVisibility(isTitleVisible ? View.VISIBLE : View.GONE);
        }
    }
    public void setMessageVisible(boolean visible){
        isMessageVisible=visible;
        if(messageTextView!=null){
            messageTextView.setVisibility(visible?View.VISIBLE:View.GONE);
        }
    }

    public void setTitleBtnVisible(boolean visible) {
        isTitleBtnVisible = visible;
        if (titleBtn != null) {
            titleBtn.setVisibility(isTitleBtnVisible ? View.VISIBLE : View.GONE);
        }
    }

    public void setTitleBtnListener(View.OnClickListener titleListener) {
        this.titleListener = titleListener;
        if (titleListener != null && titleBtn != null) {
            titleBtn.setOnClickListener(titleListener);
        }
    }

    public void addItem(String itemText, onSeparateItemClickListener listener) {
        addItem(itemText, defaultColor, listener);
    }

    public void addItem(String itemText, int color, onSeparateItemClickListener listener) {
        itemTextList.add(new Pair<String, Integer>(itemText, color));
        itemListenerList.add(listener);
        itemSize = itemTextList.size();
    }

    public void addItem(int resId, onSeparateItemClickListener listener) {
        addItem(context.getString(resId), listener);
    }

    public void addItem(int resId, int color, onSeparateItemClickListener listener) {
        addItem(context.getString(resId), color, listener);
    }

    public void addItemAfterAnother(String itemText, String another, onSeparateItemClickListener listener) {
        int index = itemTextList.indexOf(another);
        itemTextList.add(index + 1, new Pair<String, Integer>(itemText, defaultColor));
        itemListenerList.add(index + 1, listener);
        itemSize = itemTextList.size();
    }

    public void clearData() {
        itemTextList.clear();
        itemListenerList.clear();
        itemSize = 0;
    }

    private void updateListView() {
        listAdapter.notifyDataSetChanged();
        if (listView != null) {
            listView.setAdapter(listAdapter);
            listView.setOnItemClickListener(itemListener);
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
        isMessageVisible = TextUtils.isEmpty(message) ? false : true;
        setMessageVisible(isMessageVisible);
        if(messageTextView!=null){
            messageTextView.setText(message);
        }
    }

    public interface onSeparateItemClickListener {

        void onClick();
    }

  /*  @Override
    public void show() {
        if (itemSize <= 0) {
            return;
        }
        updateListView();
        super.show();
    }*/

}
