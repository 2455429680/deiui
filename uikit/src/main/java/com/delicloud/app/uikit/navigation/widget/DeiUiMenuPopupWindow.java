package com.delicloud.app.uikit.navigation.widget;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.delicloud.app.uikit.R;
import com.delicloud.app.uikit.navigation.model.DeiUiMenuPopupWindowModel;
import com.delicloud.app.uikit.utils.ScreenUtil;

import java.util.List;

/**
 * Created by Irvin
 * on 2017/8/5.
 */

public class DeiUiMenuPopupWindow extends PopupWindow {

    private Context mContext;
    private ListView popupList;
    private int windowWidth;
    private int popupWidth;


    public DeiUiMenuPopupWindow(Activity context, List<DeiUiMenuPopupWindowModel> list, PopItemTheme theme) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View contentView = inflater.inflate(R.layout.deiui_menu_popup_window, null);

        mContext = context;
        popupList = (ListView) contentView.findViewById(R.id.popup_list);
        popupList.setAdapter(new PopupListAdapter(context, list,theme));
        windowWidth = ScreenUtil.getDisplayWidth();
        setContentView(contentView);

        // 设置SelectPicPopupWindow弹出窗体的宽
//        if (windowWidth >= 1080) {
//            popupWidth = (int) (windowWidth * 0.48);
//        } else if (windowWidth <= 720) {
//            popupWidth = (int) (windowWidth * 0.45);
//        } else {
//            popupWidth = (int) (windowWidth * 0.46);
//        }
        popupWidth = ScreenUtil.dip2px(140);
        setWidth(popupWidth);
        // 设置SelectPicPopupWindow弹出窗体的高
        setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        // 设置SelectPicPopupWindow弹出窗体可点击
        setFocusable(true);
        setOutsideTouchable(true);

        // If the inflated view doesn't have a background set,
        // or the popup window itself doesn't have a background
        // set (or has a transparent background) then you won't get a shadow.
        // 必须给popup window设置不透明的背景否则没有立体的阴影
        // ColorDrawable dw = new ColorDrawable(0x80000000);
        // 点back键和其他地方使其消失,设置了这个才能触发OnDismissListener ，设置其他控件变化等操作
        //  setBackgroundDrawable(dw)
        //这里有自定义背景，不需要再设置，设置为透明
        setBackgroundDrawable(new ColorDrawable(0x00000000));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            setElevation(10);
        }
        // 刷新状态
        update();

        // 设置PopupWindow弹出窗体动画效果
        //setAnimationStyle(R.style.AnimationPreview);
    }

    public void setOnItemClick(AdapterView.OnItemClickListener itemClickListener) {
        popupList.setOnItemClickListener(itemClickListener);
    }


    private class PopupListAdapter extends BaseAdapter {
        private List<DeiUiMenuPopupWindowModel> list;
        private LayoutInflater inflater;
        private PopItemTheme popItemTheme;

        PopupListAdapter(Context context, List<DeiUiMenuPopupWindowModel> list, PopItemTheme popItemTheme) {
            inflater = LayoutInflater.from(context);
            this.list = list;
            this.popItemTheme=popItemTheme;
        }

        @Override
        public int getCount() {
            return list == null ? 0 : list.size();
        }

        @Override
        public Object getItem(int position) {
            return list.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                holder = new Holder();
                if(popItemTheme==PopItemTheme.IMG_TEXT) {
                    convertView = inflater.inflate(R.layout.deiui_menu_popup_window_item_img_text, null);
                    holder.imageItem = (ImageView) convertView.findViewById(R.id.menu_item_image);
                }else{
                    convertView=inflater.inflate(R.layout.deiui_menu_popup_window_item_text,null);
                }
                    holder.textItem = (TextView) convertView.findViewById(R.id.menu_item_text);
                    convertView.setMinimumWidth(windowWidth);
                    convertView.setTag(holder);

            } else {
                holder = (Holder) convertView.getTag();
            }
            if(popItemTheme==PopItemTheme.IMG_TEXT)
            holder.imageItem.setImageResource(list.get(position).getImageResource());
            holder.textItem.setText(list.get(position).getTextInt());
            holder.textItem.setTextColor(list.get(position).getTextColor());
            return convertView;
        }

        class Holder {
            ImageView imageItem;
            TextView textItem;
        }
    }
    public enum PopItemTheme {
        TEXT,IMG_TEXT
    }

    /**
     * 显示popupWindow
     *
     * @param parent
     */
    public void showPopupWindowAsDefault(View parent) {
        if (!isShowing()) {
            //View anchor, int xoff, int yoff
            // 相对某个控件的位置有偏移;
            // xoff表示x轴的偏移，正值表示向左，负值表示向右；
            // yoff表示相对y轴的偏移，正值是向下，负值是向上；
            showAsDropDown(parent,
                    windowWidth - popupWidth - ScreenUtil.dip2px(10), -12);
        } else {
            dismiss();
        }
    }
}
