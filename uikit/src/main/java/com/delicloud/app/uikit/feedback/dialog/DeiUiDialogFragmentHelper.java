package com.delicloud.app.uikit.feedback.dialog;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import androidx.annotation.NonNull;
import com.delicloud.app.uikit.R;


public class DeiUiDialogFragmentHelper {
    public static void popClearMessageConfirmDialog(final Activity activity, final OnClearMessageListener listener,
                                                    String title) {
        OnDialogActionListener actionListener = new OnDialogActionListener() {
            @Override
            public void doCancelAction() {
            }

            @Override
            public void doOkAction() {
                listener.clearAllMessage();
                // activity.finish();
            }
        };
        final DeiUiDialogFragment dialog = createDoubleButtonDialog(activity, null, title,
                activity.getString(R.string.clear_empty), null, true, actionListener);
    }

    public interface OnClearMessageListener {
        void clearAllMessage();
    }

    public static DeiUiDialogFragment createOneButtonDialog(Context mContext, int titleResId, int msgResId, int btnResId,
                                                            boolean cancelable, final OnClickListener positiveListener) {
        return createOneButtonDialog(mContext, getString(mContext, titleResId), getString(mContext, msgResId),
                getString(mContext, btnResId), cancelable, positiveListener);
    }

    public static DeiUiDialogFragment createOneButtonDialog(Context mContext, CharSequence titleString, CharSequence msgString,
                                                            CharSequence btnString, boolean cancelable, final OnClickListener positiveListener) {
        final DeiUiDialogFragment dialog = new DeiUiDialogFragment(mContext);
        configOneButtonDialog(dialog, mContext, titleString, msgString, btnString, cancelable, positiveListener);
        return dialog;
    }

    private static void configOneButtonDialog(final DeiUiDialogFragment dialog, Context mContext, CharSequence titleString, CharSequence msgString,
                                              CharSequence btnString, boolean cancelable, final OnClickListener positiveListener) {
        if (TextUtils.isEmpty(titleString)) {
            dialog.setTitleVisible(false);
        } else {
            dialog.setTitle(titleString);
        }
        if (TextUtils.isEmpty(msgString)) {
            dialog.setMessageVisible(false);
        } else {
            dialog.setMessage(msgString);
        }
        dialog.setCancelable(cancelable);
        dialog.addPositiveButton(TextUtils.isEmpty(btnString) ? mContext.getString(R.string.i_know) : btnString,
                DeiUiDialogFragment.NO_TEXT_COLOR, DeiUiDialogFragment.NO_TEXT_SIZE, new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        Log.d("TTTT", "点击dddddddddd了");

                        if (positiveListener != null) {
                            positiveListener.onClick(v);
                        }
                    }
                });
    }

    private static void configDoubleButtonDialog(final DeiUiDialogFragment dialog, CharSequence title, CharSequence message,
                                                 CharSequence okStr, CharSequence cancelStr, boolean cancelable, final OnDialogActionListener listener) {
        OnClickListener okListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null) {
                    listener.doOkAction();
                }
            }
        };
        OnClickListener cancelListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (listener != null) {
                    listener.doCancelAction();
                }
            }
        };
        if (TextUtils.isEmpty(title) || title == null) {
            dialog.setTitleVisible(false);
        } else {
            dialog.setTitle(title);
        }
        if (TextUtils.isEmpty(message)) {
            dialog.setMessageVisible(false);
        } else {
            dialog.setMessage(message);
        }
        dialog.setCancelable(cancelable);
        dialog.addPositiveButton(okStr, okListener);
        dialog.addNegativeButton(cancelStr, cancelListener);
    }

    public static DeiUiDialogFragment createOneButtonWithImgDialog(Context mContext, CharSequence titleString, CharSequence msgString,
                                                                   CharSequence btnString, boolean cancelable, final OnClickListener positiveListener) {
        final DeiUiDialogFragment dialog = new DeiUiDialogFragment(mContext, R.layout.deiui_alert_dialog_with_img);
        configOneButtonDialog(dialog, mContext, titleString, msgString, btnString, cancelable, positiveListener);
        return dialog;
    }

    public static DeiUiDialogFragment createDoubleButtonWithImg(final Context mContext, final CharSequence titleString, final CharSequence msgString,
                                                                final CharSequence btnString, boolean cancelable, final OnClickListener positiveListener) {
        final DeiUiDialogFragment dialog = new DeiUiDialogFragment(mContext, R.layout.deiui_alert_dialog_with_img);
        configOneButtonDialog(dialog, mContext, titleString, msgString, btnString, cancelable, positiveListener);
        return dialog;
    }


    /**
     * 两个按钮的dialog
     *
     * @param context
     * @param title      根据title是否为空设置有无标题
     * @param message
     * @param okStr
     * @param cancelStr
     * @param cancelable
     * @param listener
     * @return
     */
    private static DeiUiDialogFragment createDoubleButtonDialog(Context context, CharSequence title, CharSequence message,
                                                                CharSequence okStr, CharSequence cancelStr, boolean cancelable, final OnDialogActionListener listener) {
        final DeiUiDialogFragment dialog = new DeiUiDialogFragment(context);
        configDoubleButtonDialog(dialog, title, message, okStr, cancelStr, cancelable, listener);
        return dialog;
    }

    public static DeiUiDialogFragment createDoubleButtonWithTitleDialog(Context context, @NonNull CharSequence title, CharSequence message,
                                                                        CharSequence okStr, CharSequence cancelStr, boolean cancelable, final OnDialogActionListener listener) {
        return createDoubleButtonDialog(context, title, message, okStr, cancelStr, cancelable, listener);
    }

    public static DeiUiDialogFragment createDoubleButtonWithoutTittleDialog(Context context, CharSequence message,
                                                                            CharSequence okStr, CharSequence cancelStr, boolean cancelable, final OnDialogActionListener listener) {
        return createDoubleButtonDialog(context, null, message, okStr, cancelStr, cancelable, listener);
    }

    public interface OnDialogActionListener {
        void doCancelAction();

        void doOkAction();
    }

    private static String getString(Context context, int id) {
        if (id == 0) {
            return null;
        }
        return context.getString(id);
    }
}
